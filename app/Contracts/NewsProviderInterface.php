<?php

namespace App\Contracts;

use App\Models\Provider;

interface NewsProviderInterface
{
    public function fetch(Provider $provider);

    public function getItemsFromResponse(Provider $provider, $response): array;
}
