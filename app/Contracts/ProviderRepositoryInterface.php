<?php

namespace App\Contracts;

use App\Models\Provider;

interface ProviderRepositoryInterface
{
    public function update(Provider $provider, $current_page);
}
