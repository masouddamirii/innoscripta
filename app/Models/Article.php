<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'provider_id', 'content', 'source', 'category', 'author', 'published_at', 'last_fetched_at'
    ];

    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }

    public function scopeApiDatesFilter(Builder $query)
    {
        $query->when(request()->has('fromDate'), function (Builder $query) {
            $query->where('published_at', '>=', request('fromDate'));
        });

        $query->when(request()->has('toDate'), function (Builder $query) {
            $query->where('published_at', '<=', request('toDate'));
        });
    }

    public function scopeApiSourcesFilter(Builder $query)
    {
        $query->when(request()->has('source'), function (Builder $query) {
            $query->where('source', '=', request('source'));
        });
    }

    public function scopeApiCategoriesFilter(Builder $query)
    {
        $query->when(request()->has('category'), function (Builder $query) {
            $query->where('category', '=', request('category'));
        });
    }

    public function scopeApiAuthorFilter(Builder $query)
    {
        $query->where('author', '=', request('author'));
    }

    public function scopeApiSearchFilter(Builder $query)
    {
        $query->when(request()->has('search'), function (Builder $query) {
            $query->where(function (Builder $query) {
                $search = request('search');
                $query->orWhere('title', 'like', "%$search%")
                    ->orWhere('content', 'like', "%$search%");
            });
        });
    }
}
