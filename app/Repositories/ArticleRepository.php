<?php

namespace App\Repositories;

use App\Models\Article;
use App\Contracts\ArticleRepositoryInterface;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function storeOrUpdate(array $data)
    {
        return Article::query()->updateOrCreate([
            'title' => $data['title'],
        ], [
            'content' => $data['content'],
            'source' => $data['source'],
            'category' => $data['category'],
            'author' => $data['author'],
            'published_at' => $data['published_at'],
            'last_fetched_at' => now(),
            'provider_id' => $data['provider_id'],
        ]);
    }
}
