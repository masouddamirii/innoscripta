<?php

namespace App\Services;

use App\Models\Provider;
use Illuminate\Support\Facades\App;
use App\Repositories\ArticleRepository;
use App\Repositories\ProviderRepository;
use App\Contracts\NewsProviderInterface;

class ArticleService
{
    protected $provider, $articleRepository, $providerRepository;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
        $this->articleRepository = new ArticleRepository();
        $this->providerRepository = new ProviderRepository();
    }

    public function fetchArticles()
    {
        $lastPageFetched = $this->provider->current_page;
        return App::makeWith(NewsProviderInterface::class, ['title' => $this->provider->title])
            ->fetch($this->provider);
    }

    public function storeOrUpdateArticle($article)
    {
        $this->articleRepository->storeOrUpdate([
            'title' => $article['title'],
            'content' => $article['content'],
            'source' => $article['source'],
            'category' => $article['category'],
            'author' => $article['author'],
            'published_at' => $article['published_at'],
            'provider_id' => $article['provider_id'],
        ]);
    }

    public function fetchAndStoreOrUpdateArticles()
    {
        $response = $this->fetchArticles();
        $items = App::makeWith(NewsProviderInterface::class, ['title' => $this->provider->title])
            ->getItemsFromResponse($this->provider, $response);
        if (count($items)) {
            foreach ($items as $item) {
                $this->storeOrUpdateArticle($item);
            }
            $this->updateCurrentPage();
        } else {
            $this->resetCurrentPage();
        }
    }

    public function updateCurrentPage()
    {
        $this->providerRepository->update($this->provider, $this->provider->current_page + 1);
    }

    private function resetCurrentPage()
    {
        $this->providerRepository->update($this->provider, 1);
    }
}
