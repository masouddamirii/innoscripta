<?php

namespace App\Services;

use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Contracts\NewsProviderInterface;

class GuardianProvider implements NewsProviderInterface
{
    public function fetch(Provider $provider)
    {
        $response = Http::get($provider->base_url, [
            'page' => $provider->current_page,
            'api-key' => $provider->api_key,
            'country' => 'us'
        ]);
        return $response->json();
    }

    public function getItemsFromResponse(Provider $provider, $response): array
    {
        $results = $response['response']['results'];
        $items = [];
        foreach ($results as $result) {
            $items[] = [
                'title' => $result['sectionName'],
                'content' => $result['webTitle'] ?? 'null',
                'source' => $provider->title,
                'category' => $result['pillarName'],
                'author' => null,
                'published_at' => Carbon::make($result['webPublicationDate']) ?? now(),
                'last_fetched_at' => now()->format('Y/m/d'),
                'provider_id' => $provider->id
            ];
        }
        return $items;
    }
}
