<?php

namespace App\Services;

use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Contracts\NewsProviderInterface;

class NewsApiProvider implements NewsProviderInterface
{

    public function fetch(Provider $provider)
    {
        $response = Http::get($provider->base_url, [
            'page' => $provider->current_page,
            'apiKey' => $provider->api_key,
            'country' => 'us'
        ]);
        return $response->json();
    }

    public function getItemsFromResponse(Provider $provider, $response): array
    {
        $results = $response['articles'];
        $items = [];
        foreach ($results as $result) {
            $items[] = [
                'title' => $result['title'],
                'content' => $result['content'] ?? 'null',
                'source' => $provider->title,
                'category' => $result['source']['name'],
                'author' => $result['author'],
                'published_at' => Carbon::make($result['publishedAt']) ?? now(),
                'last_fetched_at' => now()->format('Y/m/d'),
                'provider_id' => $provider->id
            ];
        }
        return $items;
    }
}
