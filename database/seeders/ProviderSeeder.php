<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = (new Provider())->getTable();
        $now = now();
        DB::table($table)->insert([
            'title' => 'guardian',
            'base_url' => 'https://content.guardianapis.com/search',
            'api_key' => '59d8d284-fa0b-4df7-885a-4b9a370cee2b',
            'current_page' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        DB::table($table)->insert([
            'title' => 'newsapi',
            'base_url' => 'https://newsapi.org/v2/everything',
            'api_key' => 'bd18d18fbe834a9fb85b6df0a4b167c6',
            'current_page' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
